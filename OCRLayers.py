'''
Created on Aug 7, 2017

@author: busta
'''

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np

class SqLayer(Layer):

    def __init__(self, **kwargs):
        super(SqLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        super(SqLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        return K.squeeze(x, 2)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[1], input_shape[3])