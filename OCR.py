import sys
import os
sys.path.append('C:/Users/Milan/PycharmProjects/OCR')
sys.path.append('..')
os.chdir(sys.path[0])
import datetime
import numpy as np
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation
from keras.layers import Reshape, Lambda
from keras.layers.merge import add, concatenate
from keras.models import Model, load_model
from keras.layers.recurrent import GRU
from keras.optimizers import SGD
from keras.utils import plot_model
import glob
import keras
import time
from skimage import color
from skimage import io
from keras.callbacks import CSVLogger
import Utils
import tensorflow as tf
import Levenshtein
import OCRLayers
import pandas as pd

TrainParam = False
# nastaveni NN
NumEpochs = 2
NumSteps=300
# CestaData = os.getcwd()+'/Dataset'
CestaData = 'C:/Users/Milan/PycharmProjects/OCR/Dataset'
# CestaData = '/tmp/qqpultar'

# Network parameters
pool_size = 2

def DownSample(x):
    return (x // (pool_size ** 2) - 4)
def ctc_lambda_func(args):
    y_pred, labels, input_length, label_length = args
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)

def main(dirname, run_name, start_epoch, stop_epoch, img_h, codec=None):
    if K.image_data_format() == 'channels_first':
        input_shape = (2, 1,None, img_h)
    else:
        input_shape = (2, None, img_h, 1)

    act = 'relu'
    input_data = Input(name='input', batch_shape=input_shape, dtype='float32')
    inner = Conv2D(32, (3, 3), padding='same',
                   activation=act, kernel_initializer='he_normal',
                   name='conv1', use_bias=False)(input_data)
    keras.layers.normalization.BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                                                  beta_initializer='zeros', gamma_initializer='ones',
                                                  moving_mean_initializer='zeros', moving_variance_initializer='ones')
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max1')(inner)
    inner = Conv2D(64, (3, 3), padding='same',
                   activation=act, kernel_initializer='he_normal',
                   name='conv2',use_bias=False)(inner)
    keras.layers.normalization.BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                                                  beta_initializer='zeros', gamma_initializer='ones',
                                                  moving_mean_initializer='zeros', moving_variance_initializer='ones')
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max2')(inner)

    inner = Conv2D(128, (3, 3), padding='valid',
                   activation=act, kernel_initializer='he_normal',
                   name='conv3', use_bias=False)(inner)
    keras.layers.normalization.BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                                                  beta_initializer='zeros', gamma_initializer='ones',
                                                  moving_mean_initializer='zeros', moving_variance_initializer='ones')
    inner = MaxPooling2D(pool_size=(1, pool_size), name='max3')(inner)
    inner = Conv2D(256, (3, 3), padding='valid',
                   activation=act, kernel_initializer='he_normal',
                   name='conv4', use_bias=False)(inner)
    keras.layers.normalization.BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,
                                                  beta_initializer='zeros', gamma_initializer='ones',
                                                  moving_mean_initializer='zeros', moving_variance_initializer='ones')

    # TODO: sem pridat dropout

    inner = Conv2D(len(codec)+1, (3,1), padding='same',
                   activation='linear', kernel_initializer='he_normal',
                   name='conv5')(inner)
    inner = OCRLayers.SqLayer()(inner)

    y_pred = Activation('softmax', name='softmax')(inner)
    Model(inputs=input_data, outputs=y_pred).summary()

    labels = Input(name='labels', shape=[None], dtype='float32')
    input_length = Input(name='input_length', shape=[1], dtype='int64')
    label_length = Input(name='label_length', shape=[1], dtype='int64')

    loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([y_pred, labels, input_length, label_length])
    sgd = SGD(lr=0.02, decay=1e-6, momentum=0.9, nesterov=True, clipnorm=5)
    model = Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_out)

    # captures output of softmax so we can decode the output during visualization
    #test_func = K.function([input_data], [y_pred])

    if TrainParam == True:
        model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=sgd)
        modelPred = Model(inputs=[input_data], outputs=[y_pred])
        modelPred.compile(loss='binary_crossentropy',optimizer='rmsprop')
        CallFc = Utils.MojeCallback(dirname, codec, modelPred, tf.Session(), NumSteps, DownSample)

        model.fit_generator(generator=Utils.GenNextData(dirname+'/Train', codec, DownSample, Random=True), steps_per_epoch=NumSteps, epochs=NumEpochs, callbacks=[CallFc], initial_epoch=start_epoch)
        plot_model(model, to_file='model.png')
        model.save_weights('OCR.hdf5')
    else:
        model.load_weights('OCR.hdf5')
        modelPred = Model(inputs=[input_data], outputs=[y_pred])
        modelPred.compile(loss='binary_crossentropy',optimizer='rmsprop')
        Utils.PrintCompare(dirname, os.getcwd(), modelPred, codec, DownSample)


if __name__ == '__main__':
    run_name = datetime.datetime.now().strftime('%Y:%m:%d:%H:%M:%S')
    main(CestaData, run_name, 0, 20, 32, Utils.DejCodec(os.getcwd()+'/codec.txt'))