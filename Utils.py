import torch, os, glob, random, time, editdistance, sys
import numpy as np
from itertools import chain
from skimage import io
from shutil import copyfile
import pandas as pd
from skimage import color
from PIL import Image, ImageOps
from random import randint
import diff_match_patch as DMP
from prettytable import PrettyTable
from torch.autograd import Variable
from warpctc_pytorch import CTCLoss
from visualize import make_dot
import matplotlib.pyplot as plt
from math import floor
import cv2
from DataEnqueuer import GeneratorEnqueuer

class GetOutOfLoop(Exception): pass
class Codec:
    @staticmethod
    def Create(dirs):
        codec = set(' ')
        for dir in dirs:
            if os.path.isdir(dir):
                for root, dirs, files in os.walk(dir):
                    for file in files:
                        if not file.endswith('.txt'): continue
                        text = list(open(root+'/'+file,'r', encoding='utf8').readline())
                        codec.update([pom for pom in text if not pom in chain(*codec) and not pom.isspace()])
            elif os.path.isfile(dir):
                for ln in open(dir,'r').readlines():
                    ln = ln.strip()[ln.find(' ')+1:]
                    codec.update([pom for pom in ln if not pom in chain(*codec) and not pom.isspace()])
        print('Codec created')
        return codec
    @staticmethod
    def Save(OutPath, codec):
        with open(OutPath,'w', encoding='utf8') as textF:
            for i,c in enumerate(sorted(codec)): textF.write(c+' '+str(i)+'\n')
        print('Codec saved')
    @staticmethod
    def Get(dirname):
        codec = {' ': 0}
        with open(dirname,'r',encoding='utf8') as TextF:
            lines = [c.strip() for c in TextF.readlines()]
            for ln in lines[1:]: codec[ln.split(' ')[0]] = int(ln.split(' ')[1])
        return codec
def CTC_Decode(probs, codec):
    InvCodec = dict(zip(codec.values(), codec.keys()))
    outs = [list(ln).index(max(ln)) for ln in probs.data]
    output = ''
    blank = True
    for (i,n) in enumerate(outs):
        if n == 0:
            blank = True
            continue
        if blank == True:
            blank = False
            output += InvCodec[n-1]
        else:
            if i>0 and outs[i-1]!=n: output += InvCodec[n-1]
    assert(output == CTC_DecodeNew(probs, codec))
    return output
def CTC_DecodeNew(probs, codec, blank=0): # works only for blank==0
    InvCodec = dict(zip(codec.values(), codec.keys()))
    outs = [list(ln).index(max(ln)) for ln in probs.data]
    output = ''
    for (i,n) in enumerate(outs):
        if n!=blank and ((i==0) or (i!=0 and outs[i-1]!=n)): output += InvCodec[n-1]
    return output
def CreatePrettyTable(file=None):
    if file == None: file = 'log.txt'
    PT = PrettyTable()
    with open(file,'r') as textF:
        PT.field_names = textF.readline().strip().split(',')
        for ln in textF.readlines(): PT.add_row(ln.strip().split(','))
    PT.add_row(['-']*len(PT.field_names))
    PT.add_row(PT.field_names)
    if file != 'log.txt':
        dir = os.path.dirname(file)
        open(dir+'/log_tab.txt','w').write(PT.get_string())
    else: open('log_tab.txt','w').write(PT.get_string())
class Logger:
    def __init__(self, model, ListTrain, ListTest, codec, NumSteps, Grayscale=False):
        self.model = model
        self.ListTrain = ListTrain
        self.ListTest = ListTest
        self.codec = codec
        self.NumSteps = NumSteps
        self.Grayscale = Grayscale
        self.TrainLev, self.TestLev, self.TrainLevAcc, self.TestLevAcc = list(), list(), list(), list()
        self.TrainSenAcc, self.TestSenAcc, self.TrainWordAcc, self.TestWordAcc = list(), list(), list(), list()
    def Evaluate(self, Type, NumSamples=-1):
        ListData = self.ListTrain if Type=='Train' else self.ListTest
        self.model.eval()
        RSen, NumSen, RWord, NumWord, Lev, LKolik, NumWords = 0, 0, 0, 0, 0, 0, 0
        if NumSamples == -1: gen = Generator(self.codec, Grayscale=self.Grayscale).GetOne(ListData, Random = False, Infty=False)
        else: gen = Generator(self.codec, Grayscale=self.Grayscale).GetOne(ListData, Random=False, samples=NumSamples) # change 6.9.
        for (i,nxt) in enumerate(gen):
            input = Variable(torch.from_numpy(np.expand_dims(nxt['input'], 0)).float())
            new = CTC_Decode(self.model(input, self.codec).squeeze().permute(1,0), self.codec)
            Lev += editdistance.eval(new, nxt['source'])
            LKolik += len(nxt['source'])
            if new == nxt['source']: RSen += 1
            RWord += PrintPredicts.HowManyWordsRight(nxt['source'], new)
            NumSen += 1
            NumWord += len(nxt['source'].split(' '))
            if (i+1) == NumSamples: break
        self.model.train()
        return (Lev, (LKolik-Lev)/LKolik, RSen/NumSen, RWord/NumWord)
    def EpochEnd(self, AvgLoss, epoch):
        (Lev, LevAcc, SenAcc, WordAcc) = self.Evaluate('Train',30)
        self.TrainLev.append(Lev)
        self.TrainLevAcc.append(LevAcc)
        self.TrainSenAcc.append(SenAcc)
        self.TrainWordAcc.append(WordAcc)
        (Lev, LevAcc, SenAcc, WordAcc) = self.Evaluate('Test',20)
        self.TestLev.append(Lev)
        self.TestLevAcc.append(LevAcc)
        self.TestSenAcc.append(SenAcc)
        self.TestWordAcc.append(WordAcc)
        out = [epoch,self.NumSteps,AvgLoss,self.TrainLev[-1],self.TestLev[-1],time.time() - self.StartTime,self.TrainSenAcc[-1],self.TestSenAcc[-1],self.TrainWordAcc[-1],
               self.TestWordAcc[-1],self.TrainLevAcc[-1],self.TestLevAcc[-1]]
        with open('log.txt','a') as textF: textF.write(','.join([str(round(n,5)) for n in out])+'\n')
    def TrainBegin(self):
        self.StartTime = time.time()
        with open('log.txt','w') as textF: textF.write('Epoch,EpochSize,CTCLoss,TrainLev,TestLev,Time,TrainSenAcc,TestSenAcc,TrainWordAcc,TestWordAcc,TrainLevAcc,TestLevAcc\n')
    def TrainEnd(self, OutName = 'log.txt'):
        (Lev, LevAcc, SenAcc, WordAcc) = self.Evaluate('Train',-1)
        self.TrainLev.append(Lev)
        self.TrainLevAcc.append(LevAcc)
        self.TrainSenAcc.append(SenAcc)
        self.TrainWordAcc.append(WordAcc)
        (Lev, LevAcc, SenAcc, WordAcc) = self.Evaluate('Test',-1)
        self.TestLev.append(Lev)
        self.TestLevAcc.append(LevAcc)
        self.TestSenAcc.append(SenAcc)
        self.TestWordAcc.append(WordAcc)
        out = [-1,-1,-1,self.TrainLev[-1],self.TestLev[-1],time.time() - self.StartTime,self.TrainSenAcc[-1],self.TestSenAcc[-1],self.TrainWordAcc[-1],self.TestWordAcc[-1],
               self.TrainLevAcc[-1],self.TestLevAcc[-1]]
        with open(OutName,'a') as textF: textF.write(','.join([str(round(n,5)) for n in out]) + '\n')
        CreatePrettyTable()
    def WriteSummary(self, OutName = 'log_sum.txt'):
        (Lev, LevAcc, SenAcc, WordAcc) = self.Evaluate('Test',-1)
        OutTest = [Lev, LevAcc, SenAcc, WordAcc]
        out = [OutTest[0],OutTest[2],OutTest[3],OutTest[1]]
        with open(OutName,'w') as textF:
            textF.write('TestLev,TestSenAcc,TestWordAcc,TestLevAcc\n')
            textF.write(','.join([str(round(n,5)) for n in out]) + '\n')
class Generator: # GiveBatch always gives random samples
    def __init__(self, codec, Invert=False, Grayscale=False):
        self.Invert = Invert
        self.Grayscale = Grayscale
        self.codec = codec
        # self.seznam = sorted(glob.glob(self.DirData+"/*png"))  # we assume that .txt files have each the same name as the corresponding .png file
    def GenOne(self, ListData, Random=False, Infty=True, samples=-1): # the batch/first dimension is missing here, we use this in printing methods
        gen = GeneratorEnqueuer(self.GetOne(ListData=ListData, Random=Random, Infty=Infty, samples=samples))
        gen.start()
        return gen.get()
    def GenBatch(self, ListData, DownFactor, BatchSize=1, Groups=10): # Random=True, Infty=True
        gen = GeneratorEnqueuer(self.GetBatch(ListData=ListData, DownFactor=DownFactor, BatchSize=BatchSize, Groups=Groups))
        gen.start()
        return gen.get()
    def GetOne(self, ListData, Random=False, Infty=True, samples=-1): # the batch/first dimension is missing here, we use this in printing methods
        if Random==True:
            while True and samples != 0:
                nxt = None
                while nxt == None: nxt = self.Get(random.choice(ListData), self.codec)
                # yield self.Get(random.choice(ListData), self.codec)
                yield nxt
                if samples != -1: samples -= 1
        elif Random==False:
            try:
                while True:
                    for i,sample in enumerate(ListData):
                        try:
                            nxt = self.Get(sample, self.codec)
                            yield nxt
                        except: pass
                        if (i+1)==samples: raise GetOutOfLoop
                    if Infty==False: raise GetOutOfLoop
            except GetOutOfLoop: pass
    def GetBatch(self, ListData, DownFactor, BatchSize=1, Groups=10): # Random=True, Infty=True
        Lists = self.CreateLists(ListData, Groups) # Creates lists of pictures of approx. the same width
        while True:
            OutData = {'input': list(), 'input_length': list(), 'labels': list(), 'label_length': list(), 'source': list(),  'name': list(), 'file': list() }
            which = randint(0, len(Lists)-1)
            for i in range(BatchSize):
                nxt = None
                while nxt == None: nxt = self.Get(random.choice(Lists[which]), self.codec)
                # nxt = self.Get(random.choice(Lists[which]), self.codec) # change 4.9.
                for ln in nxt.keys(): OutData[ln].append(nxt[ln])
            for i,ln in enumerate(OutData['input']):
                OutData['input_length'].append(DownFactor(OutData['input'][i].shape[2]))
                pom = np.zeros_like(np.resize(ln, max([a.shape for a in OutData['input']])))
                pom[:OutData['input'][i].shape[0], :OutData['input'][i].shape[1], :OutData['input'][i].shape[2]] = OutData['input'][i]
                OutData['input'][i] = pom
            OutData['input_length'] = np.asarray(OutData['input_length'])
            OutData['input'] = np.asarray(OutData['input'])
            pom = list()
            for i,ln in enumerate(OutData['labels']): pom += list(ln)
            OutData['labels'] = np.asarray(pom)
            OutData['label_length'] = np.asarray(OutData['label_length'])
            yield OutData
    def Get(self, sample, codec):
        try:
            y = list()
            file=sample[0]
            name = os.path.splitext(os.path.basename(file))[0]
            if self.Grayscale == True: img = Image.open(file).convert('L')
            else: img = Image.open(file).convert('RGB')
            img = img.resize((int(img.size[0] / (img.size[1] / 32)), 32), resample=Image.BILINEAR)
            if self.Invert==True and randint(0,1) == 1: img = ImageOps.invert(img)
            if self.Grayscale == True: img = np.expand_dims(np.asarray(img), 2)
            # cv2.imshow("Display window", img)
            # cv2.waitKey(0)
            img = np.swapaxes(np.asarray(img), 0,2)
            img = np.swapaxes(img, 1,2)
            img = np.asarray(img / 128 - 1)

            source_str = sample[1]
            y = list(source_str)
            label_length = len(y)
            try: # the codec might not cover all symbols in the dataset
                for j in range(len(y)): y[j] = codec[y[j]]
                y = np.asarray(y) + 1
            except: pass
            return {'input': img, 'labels': y, 'label_length': label_length, 'source': source_str,  'name': name, 'file': file }
        except: return None
    @staticmethod
    def CreateLists(ListData, Groups=10):
        samples, out = list(), list()
        indexes = [floor((len(ListData) / Groups) * a) for a in range(Groups)]
        for sample in ListData:
            try: samples.append((Image.open(sample[0]).size[0],sample))
            except: pass
        samples.sort(key=lambda x: x[0])
        where = -1
        for i,(_,ln) in enumerate(samples):
            if i in indexes:
                where+=1
                out.append(list())
            out[where].append(ln)
        return out
class PrintPredicts:
    def __init__(self, Grayscale):
        self.Grayscale = Grayscale
    def PrintCompare(self, DirOut, modelPred, codec, ListTrain=[], ListTest=[]): # can be changed to __call__
        pd.set_option('display.max_colwidth', -1)
        if len(ListTrain)!= 0: self.Print(ListTrain, DirOut, modelPred, codec, 'Train')
        if len(ListTest) != 0: self.Print(ListTest, DirOut, modelPred, codec, 'Test')
    def Print(self, DirData, DirOut, model, codec, Type):
        df = pd.DataFrame(columns=['Correct','WsR', 'WsW','Lev.', 'CTC_loss','Source','Prediction','Img'])
        WList = list()
        ctc_loss = CTCLoss()
        for (j,nxt) in enumerate(Generator(DirData+'/'+Type, codec, Invert = False, Grayscale=self.Grayscale).GetOne(Infty=False)):
            pred = model(Variable(torch.from_numpy(np.expand_dims(nxt['input'], 0)).float()), codec)
            new = CTC_Decode(pred.squeeze().permute(1,0), codec)
            if new!=nxt['source']:
                loss = Variable(torch.FloatTensor([-1])) # because of the try block, codec might be shorter
                try:
                    labels = Variable(torch.from_numpy(nxt['labels']).int())
                    probs_sizes = Variable(torch.IntTensor( [pred.size()[2]]) )
                    label_sizes = Variable(torch.IntTensor( [len(labels)]) )
                    loss = ctc_loss(pred.permute(2,0,1), labels, probs_sizes, label_sizes)
                except: pass # we end up here if codec used during training does not cover all symbols
                Lev = editdistance.eval(new,nxt['source'])
                WList.append((Lev, (new, nxt, loss.data[0]))) # change 1.9.
            if (j+1)%10 == 0: print('Seen '+Type+' sample '+str(j+1))
        for (i, (Lev, (new, nxt, loss))) in enumerate(sorted(WList, key=lambda x: x[0], reverse=True)):
            name = r'<img style="display:block;" height="100%" src="'+Type+'/'+nxt['name']+r'.png"/>'
            right = PrintPredicts.HowManyWordsRight(nxt['source'],new)
            HighLTPred = PrintPredicts.GetHighLTText(nxt['source'], new)
            df.loc[i] = [new==nxt['source'], right, len(nxt['source'].split(' '))-right, Lev,loss, nxt['source'], HighLTPred, name]
            if (i+1)%10 == 0: print(Type+' done '+str(i+1)) # change 1.9.
        with open(DirOut+'/'+Type+'Outs.html','w') as textF: textF.write(df.to_html(escape=False))
    @staticmethod
    def HowManyWordsRight(GT, pred):
        df = DMP.diff_match_patch()
        df = [a[1] for a in df.diff_main(GT, pred) if a[0] == 0]
        search = list()
        for a in df:
            for b in (a.strip()).split(' '):
                search.append(b)
        lns = GT.split(' ')
        right = 0
        for a in search:
            if lns.__contains__(a): right += 1
        return right
    @staticmethod
    def GetHighLTText(GT, Pred):
        outPred = ''
        df = DMP.diff_match_patch()
        for pom in df.diff_main(Pred, GT):
            if pom[0]==0: outPred += pom[1] # change 1.9.
            elif pom[0]==(-1): outPred += r'<span style="background-color:  #ff9999">'+pom[1]+r'</span>' # change 1.9.
        return outPred
class ModelWrap: # this wraps the modln[ln.find(' ')+1:-1]el so you can Train it or Test it
    def __init__(self, model, codec, Grayscale=False):
        self.model = model
        self.codec = codec
        self.Grayscale = Grayscale
    def Train(self, DownFactor, NumEpochs, NumSteps, optimizer, DirsListTrain, DirsListTest, clip_norm=50, RandomInvert=False, BatchSize=1, Groups=10, TrainToTest=None):
        self.model.train()
        ListTrain = ModelWrap.LoadList(DirsListTrain)
        ListTest = ModelWrap.LoadList(DirsListTest)
        if TrainToTest is not None:
            number = int(TrainToTest * len(ListTrain))
            for i in range(number):
                rand = randint(0,len(ListTrain)-1)
                ListTest.append(ListTrain[rand])
                del ListTrain[rand]
        try: self.model.load_state_dict(torch.load('starter_'+os.path.basename(sys.argv[0]).split('.')[0]+'.state'))
        except: pass
        ctc_loss = CTCLoss()
        InputData = next(Generator(self.codec, Grayscale=self.Grayscale).GetOne(ListTrain)) # change 1.9.
        make_dot(self.model(Variable(torch.from_numpy(np.expand_dims(InputData['input'], 0)).float()), self.codec)).render(os.path.basename(sys.argv[0]).split('.')[0])
        gen = Generator(self.codec, Invert=RandomInvert,Grayscale=self.Grayscale).GenBatch(ListTrain, DownFactor, BatchSize=BatchSize, Groups=Groups)
        logger = Logger(self.model, ListTrain, ListTest, self.codec, NumSteps, Grayscale=self.Grayscale)#, Generator(self.DirData, self.codec, Invert=False, Grayscale=self.Grayscale))
        logger.TrainBegin()
        try:
            for epoch in range(NumEpochs):
                for PG in optimizer.param_groups: PG['lr']*=0.9
                print('Epoch {}/{}'.format(epoch+1, NumEpochs))
                running_loss = 0.0
                for i in range(NumSteps): # zde by slo dat pouze enumarate(gen) ... vyzkouset
                    InputData = next(gen)
                    labels = Variable(torch.from_numpy(InputData['labels']).int())
                    inputs = Variable(torch.from_numpy(InputData['input']).float())
                    optimizer.zero_grad()
                    try: outputs = self.model(inputs, self.codec)
                    except: continue
                    probs_sizes = Variable(torch.IntTensor( [(outputs.permute(2,0,1).size()[0])] * (outputs.permute(2,0,1).size()[1]) ))
                    label_sizes = Variable(torch.IntTensor( torch.from_numpy(InputData['label_length']).int() ))
                    loss = ctc_loss(outputs.permute(2,0,1), labels, probs_sizes, label_sizes) / BatchSize # change 1.9.
                    loss.backward()
                    torch.nn.utils.clip_grad_norm(self.model.parameters(), clip_norm)
                    optimizer.step()
                    if (i+1)%10==0: print('\rsample {}/{}, ctc_loss: {:.3f}'.format(i+1,NumSteps,loss.data[0]),end='')
                    running_loss += loss.data[0]
                print('\rAvg. loss: {:.3f}                  '.format(running_loss / NumSteps))
                logger.EpochEnd(running_loss / NumSteps, epoch+1)
        except KeyboardInterrupt: # CHANGE 4.9.
            torch.save(self.model.state_dict(), os.path.basename(sys.argv[0]).split('.')[0]+'.state')
            print('Stopped early, state saved')
        logger.TrainEnd()
        print('Finished Training')
        torch.save(self.model.state_dict(), os.path.basename(sys.argv[0]).split('.')[0]+'.state')
        print('State saved')
    def Test(self, Steps, DirsListTrain, DirsListTest, StateDict=None, Eval=True):
        self.model.eval()
        ListTrain = ModelWrap.LoadList(DirsListTrain)
        ListTest  = ModelWrap.LoadList(DirsListTest)
        if StateDict!= None: self.model.load_state_dict(StateDict)
        if Eval==True: Logger(self.model, self.codec, Steps, Grayscale=self.Grayscale, ListTrain=ListTrain, ListTest=ListTest).WriteSummary()
        PrintPredicts(self.Grayscale).PrintCompare(self.DirData, os.getcwd(), self.model, self.codec)
    @staticmethod
    def LoadList(DirsList, comma=False, quotes=False): # this supports different formats
        out = list()
        for ActDir in DirsList:
            for ln in open(ActDir, 'r').readlines():
                ln = ln.strip()
                FShift = 1
                BEnd = len(ln)
                if comma  == True: FShift += 1
                if quotes == True:
                    FShift  += 1
                    BEnd   -= 1
                out.append((os.path.dirname(ActDir)+'/'+ln.split()[0], ln[ln.find(' ')+FShift:BEnd]))
        return out
if __name__ == '__main__': Codec.Save(os.getcwd()+'/codec.txt', Codec.Create(['/home/milan/Milan/OCR_Pytorch/Datasets/Amazon/ListTrain.txt']))
    # VytvorDataset(os.getcwd()+'/book', 300)
    # CreateChineseDataset('C:/Users/Milan/PycharmProjects/OCR/CinstinaData')
    # cesta = 'C:/Milan/Skola/2016.2017/Bakalarka/RkoAnalyzeData/2017-08-21-SloucenaDataMCAP/A-pom'
    # for file in glob.glob(cesta+'/*.odhady'):
    #     name = os.path.basename(file).split('.')[0]
    #     ind = int(name.split('-')[1])
    #     dir = os.path.dirname(file)
    #     os.rename(file, dir+'/'+name.split('-')[0]+'-'+str(ind + 40)+'.mh_Funkce.odhady')
