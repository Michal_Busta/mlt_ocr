import sys
import os
sys.path.append('C:/Users/Milan/PycharmProjects/OCR')
sys.path.append('..')
os.chdir(sys.path[0])
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation
from keras.layers import Reshape, Lambda
from keras.layers.merge import add, concatenate
from keras.models import Model, load_model
from keras.layers.recurrent import GRU
from keras.optimizers import SGD, Adadelta
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Dropout
from keras.utils import plot_model
import keras
import Utils
import tensorflow as tf
import OCRLayers

TrainParam = True
NumEpochs = 120
NumSteps=300
CestaData = 'C:/Users/Milan/PycharmProjects/OCR/Dataset'
#CestaData = '/tmp/qqpultar'

# Network parameters
pool_size = 2

def DownSample(x): return (x // (pool_size**2) - 2)
def ctc_lambda_func(args): return K.ctc_batch_cost(*args)
def NactiVahy(model, img_h, codec):
    try:
        # open('Starter_'+os.path.basename(sys.argv[0]),'r')
        OCR = __import__('Starter_'+os.path.basename(sys.argv[0]).split('.')[0])
        ModelPom = OCR.DejModel(img_h,codec)
        ModelPom[0].load_weights('Starter_'+os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
        model.get_layer('conv1').set_weights(ModelPom[0].get_layer('conv1').get_weights())
        model.get_layer('conv2').set_weights(ModelPom[0].get_layer('conv2').get_weights())
        model.get_layer('conv3').set_weights(ModelPom[0].get_layer('conv3').get_weights())
        model.get_layer('conv4').set_weights(ModelPom[0].get_layer('conv4').get_weights())
        model.get_layer('conv5').set_weights(ModelPom[0].get_layer('conv5').get_weights())
        model.get_layer('batch1').set_weights(ModelPom[0].get_layer('batch1').get_weights())
        model.get_layer('batch2').set_weights(ModelPom[0].get_layer('batch2').get_weights())
        model.get_layer('batch3').set_weights(ModelPom[0].get_layer('batch3').get_weights())
        print('Weights loaded')
    except:
        print('Weights NOT loaded')
        pass
def DejModel(img_h, codec, NacistVahy=True):
    if K.image_data_format() == 'channels_first':
        input_shape = (2, 1,None, img_h)
    else: input_shape = (2, None, img_h, 1)

    # act = 'relu'
    input_data = Input(name='input', batch_shape=input_shape, dtype='float32')
    inner = Conv2D(32, (3, 3), padding='same', kernel_initializer='he_normal',name='conv1', use_bias=False)(input_data)
    inner = Conv2D(32, (3, 3), padding='same', kernel_initializer='he_normal',name='conv2', use_bias=False)(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), padding='same', name='max1')(inner)
    inner = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_normal',name='conv3', use_bias=False)(inner)
    inner = BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,beta_initializer='zeros', gamma_initializer='ones',name='batch1')(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    conv4 = Conv2D(64, (3, 3), padding='same', kernel_initializer='he_normal',name='conv4', use_bias=False)
    inner = conv4(inner)
    inner = conv4(inner)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), padding='same', name='max2')(inner)
    inner = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal',name='conv5',use_bias=False)(inner)
    inner = BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True,beta_initializer='zeros', gamma_initializer='ones',name='batch2')(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    conv6 = Conv2D(128, (3, 3), padding='same', kernel_initializer='he_normal',name='conv6', use_bias=False)
    inner = conv6(inner)
    inner = conv6(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size),strides=(1,2), padding='same', name='max3')(inner)
    inner = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal',name='conv7', use_bias=False)(inner)
    inner = BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones',name='batch3')(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    conv8 = Conv2D(256, (3, 3), padding='same', kernel_initializer='he_normal',name='conv8', use_bias=False)
    inner = conv8(inner)
    inner = conv8(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size),strides=(1,2), padding='same', name='max4')(inner)
    inner = Conv2D(512, (3, 2), padding='valid', kernel_initializer='he_normal',name='conv9', use_bias=False)(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    inner = Conv2D(512, (5, 1), padding='same', kernel_initializer='he_normal',name='conv10', use_bias=False)(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    inner = Dropout(rate=0.5, noise_shape=None, seed=None)(inner)
    inner = Conv2D(len(codec)+1, (7, 1), padding='same', kernel_initializer='he_normal',name='conv11', use_bias=False)(inner)
    inner = LeakyReLU(alpha=0.3)(inner)
    inner = OCRLayers.SqLayer()(inner)
    y_pred = Activation('softmax', name='softmax')(inner)

    labels = Input(name='labels', shape=[None], dtype='float32')
    input_length = Input(name='input_length', shape=[1], dtype='int64')
    label_length = Input(name='label_length', shape=[1], dtype='int64')

    loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([labels, y_pred, input_length, label_length])
    # sgd = SGD(lr=0.02, decay=1e-6, momentum=0.9, nesterov=True, clipnorm=5)
    model = Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_out)
    model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=Adadelta())

    if NacistVahy == True: NactiVahy(model, img_h, codec)
    return (model, input_data, y_pred)
def main(DirData, img_h, codec):
    (model, input_data, y_pred) = DejModel(img_h, codec, NacistVahy=True)
    Model(inputs=input_data, outputs=y_pred).summary()
    if TrainParam == True:
        modelPred = Model(inputs=[input_data], outputs=[y_pred])
        modelPred.compile(loss='binary_crossentropy',optimizer='rmsprop')
        CallFc = Utils.MojeCallback(DirData, codec, modelPred, tf.Session(), NumSteps, DownSample)
        model.fit_generator(generator=Utils.GenNextData(DirData+'/Train', codec, DownSample, Random=True), steps_per_epoch=NumSteps, epochs=NumEpochs, callbacks=[CallFc])
        plot_model(model, to_file='model.png')
        model.save_weights(os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
    else:
        model.save_weights(os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
        modelPred = Model(inputs=[input_data], outputs=[y_pred])
        modelPred.compile(loss='binary_crossentropy',optimizer='rmsprop')
        Utils.PrintCompare(DirData, os.getcwd(), modelPred, codec, DownSample)
if __name__ == '__main__': main(CestaData, 32, Utils.DejCodec(os.getcwd()+'/codec.txt'))