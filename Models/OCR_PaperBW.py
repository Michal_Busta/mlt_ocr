import torch, sys, os
sys.path.append('/home/milan/Milan/OCR_Pytorch')
sys.path.append('/mnt/textspotter/software/opencv/ReleaseP3/lib/python3')
sys.path.append('..')
sys.path.append('../..')
os.chdir(sys.path[0])
from Utils import Codec, Generator, Logger, ModelWrap
from torch.nn import MaxPool2d, LeakyReLU, Conv2d, BatchNorm2d, Dropout2d, Softmax2d, InstanceNorm2d, LogSoftmax
from torch.optim import SGD, Adadelta, Adagrad
from torch.nn.init import normal, xavier_normal
import torch.nn as nn
class Mode: Train, Test = 1, 0

ModelMode   =   Mode.Train
Epochs      =   80
Steps       =   900
BatchSize   =   4
Groups      =   20 # groups of pictures of different width
RandomInvert=   True
Grayscale   =   True
clip_norm   =   50
TrainToTest =   0.1 # how many samples to move from Train data to Test data (between 0,1)

DatasetName = 'Dataset_EnRu_1700_600'
#DatasetName= 'icdar2013-recognition'
if os.getcwd().split('/')[1] == 'home': CestaData = '/home/milan/Milan/OCR_Pytorch/Datasets/'+DatasetName
else: CestaData = '/tmp/qqpultar/'+DatasetName # e.g. at Halmos

# DirListTrain= [CestaData+'/ListTrain.txt']
# DirListTest = [CestaData+'/ListTest.txt']
# DirListTrain= ['/mnt/datagrid/personal/qqpultar/OCR_Pytorch/Datasets/Amazon/ListTrain.txt']
DirListTrain= ['/home/milan/Milan/OCR_Pytorch/Datasets/Amazon/ListTrain.txt']
DirListTest = []

codec       = Codec.Get('/home/milan/Milan/OCR_Pytorch/Datasets/Amazon/codec.txt')
# codec       = Codec.Get('/mnt/datagrid/personal/qqpultar/OCR_Pytorch/Datasets/Amazon/codec.txt')

def DownFactor(x): return x // 4 + 2
class GetModel(nn.Module):
    def __init__(self, codec):
        super(GetModel, self).__init__()
        if Grayscale==True: self.conv1 = Conv2d(1, 32, (3,3), padding=1)
        else: self.conv1 = Conv2d(3, 32, (3,3), padding=1)
        self.conv2 = Conv2d(64, 64, (3,3), padding=1)
        self.conv3 = Conv2d(64, 64, (3,3), padding=1) # the only diff from OCR_Paper.py
        self.conv4 = Conv2d(64, 64, (3,3), padding=1)
        self.conv5 = Conv2d(64, 128, (3,3), padding=1)
        self.conv6 = Conv2d(128, 128, (3,3), padding=1)
        self.conv7 = Conv2d(128,256, (3,3), padding=1)
        self.conv8 = Conv2d(256, 256, (3,3), padding=1)
        self.conv9 = Conv2d(256, 512, (2,3), padding=(0,1))
        self.conv10 = Conv2d(512, 512, (1,5), padding=(0,2))
        self.conv11 = Conv2d(512, len(codec)+1, (1,7), padding=(0,3))
        # self.batch1 = BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=False)
        # self.batch2 = BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=False)
        # self.batch3 = BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=False)
        self.batch1 = InstanceNorm2d(64, eps=1e-05, momentum=0.1, affine=True)
        self.batch2 = InstanceNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
        self.batch3 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
        self.drop1 = Dropout2d(p=0.2, inplace=False)
        self.leaky = LeakyReLU(negative_slope=0.01, inplace=False)
        self.max1 = MaxPool2d((2, 2), stride=None)
        self.max2 = MaxPool2d((2, 2), stride=(2,1), padding=(0,1))
        for conv in {self.conv1, self.conv2, self.conv3, self.conv4, self.conv5, self.conv6, self.conv7, self.conv8, self.conv9, self.conv10, self.conv11}:
            xavier_normal(conv.weight.data)
            normal(conv.bias.data)
    def forward(self, x, codec):
        try: x = x.cuda(0)
        except: pass
        x = self.conv1(x)
        x = torch.cat((x, x.mul(-1)), dim=1) # the only diff from OCR_Paper.py
        x = self.leaky(x)
        x = self.conv2(x)
        x = self.leaky(x)
        x = self.max1(x)
        x = self.conv3(x)
        x = self.batch1(x)
        x = self.leaky(x)
        x = self.conv4(self.conv4(x))
        x = self.leaky(x)
        x = self.max1(x)
        x = self.conv5(x)
        x = self.batch2(x)
        x = self.leaky(x)
        x = self.conv6(self.conv6(x))
        x = self.leaky(x)
        x = self.max2(x)
        x = self.conv7(x)
        x = self.batch3(x)
        x = self.leaky(x)
        x = self.conv8(self.conv8(x))
        x = self.leaky(x)
        x = self.max2(x)
        x = self.conv9(x)
        x = self.leaky(x)
        x = self.conv10(x)
        x = self.leaky(x)
        # x = self.drop1(x)model
        x = self.conv11(x)
        x = x.squeeze(2)
        x = x.permute(0,2,1)
        y = x
        x = x.contiguous().view(-1,x.data.shape[2])
        x = LogSoftmax()(x)
        x = x.contiguous().view_as(y)
        x = x.permute(0,2,1)
        return x
def Train(DirData, codec):
    model = GetModel(codec)
    try: model = model.cuda(0)
    except: pass
    # optimizer = torch.optim.Adadelta(model.parameters(),lr=1, rho=0.9, eps=1e-06, weight_decay=0.0)
    # optimizer = torch.optim.Adagrad(model.parameters(), lr=0.01, lr_decay=0, weight_decay=0)
    optimizer   = SGD(model.parameters(), lr = 0.001, momentum=0.9, nesterov=True)
    model = ModelWrap(model,codec,Grayscale=Grayscale)
    model.Train(DownFactor,Epochs,Steps,optimizer,DirListTrain,DirListTest,clip_norm=clip_norm,RandomInvert=RandomInvert,BatchSize=BatchSize,Groups=Groups,TrainToTest=TrainToTest)
def Test(DirData, codec, Eval=False):
    try: model = GetModel(codec).cuda(0)
    except: model = GetModel(codec)
    try: model.load_state_dict(torch.load(os.path.basename(sys.argv[0]).split('.')[0]+'.state'))
    except: model.load_state_dict(torch.load(os.path.basename(sys.argv[0]).split('.')[0]+'.state', map_location=lambda storage, loc: storage))
    model = ModelWrap(model, DirData, codec, Grayscale=Grayscale)
    model.Test(Steps, DirListTest=CestaData+'/ListTest.txt', Eval=Eval)
if __name__ == '__main__':
    if   ModelMode == Mode.Train: Train(CestaData, codec)
    elif ModelMode == Mode.Test : Test(CestaData, codec, Eval=True) # if Eval, then we compute accuracy over the dataset