import sys
import os
sys.path.append('C:/Users/Milan/PycharmProjects/OCR')
sys.path.append('..')
os.chdir(sys.path[0])
import datetime
import numpy as np
from keras import backend as K
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.layers import Input, Dense, Activation
from keras.layers import Reshape, Lambda
from keras.layers.core import Dropout
from keras.layers.merge import add, concatenate
from keras.models import Model, load_model
from keras.layers.recurrent import GRU
from keras.optimizers import SGD
from keras.utils import plot_model
import glob
import keras
import time
from skimage import color
from skimage import io
from keras.callbacks import CSVLogger
import Utils
import tensorflow as tf
import Levenshtein
import OCRLayers
import pandas as pd

K.set_learning_phase(False)
NumEpochs = 60
NumSteps=300
CestaData = 'C:/Users/Milan/PycharmProjects/OCR/Dataset'
# CestaData = '/tmp/qqpultar'

# Network parameters
pool_size = 2
RandomInvert = False
rnn_size = 512

def DownSample(x): return (x // (pool_size ** 2) - 4)
def ctc_lambda_func(args): return K.ctc_batch_cost(*args)
def NactiVahy(model, img_h, codec):
    try: model.load_weights(os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
    except: pass
    try:
        OCR = __import__('Starter_'+os.path.basename(sys.argv[0]).split('.')[0])
        ModelPom = OCR.DejModel(img_h,codec)
        ModelPom[0].load_weights('Starter_'+os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
        model.get_layer('conv1').set_weights(ModelPom[0].get_layer('conv1').get_weights())
        model.get_layer('conv2').set_weights(ModelPom[0].get_layer('conv2').get_weights())
        model.get_layer('conv3').set_weights(ModelPom[0].get_layer('conv3').get_weights())
        model.get_layer('conv4').set_weights(ModelPom[0].get_layer('conv4').get_weights())
        model.get_layer('conv5').set_weights(ModelPom[0].get_layer('conv5').get_weights())
    except: return
def DejModel(img_h, codec, NacistVahy=False):
    if K.image_data_format() == 'channels_first':
        input_shape = (2, 1,None, img_h)
    else: input_shape = (2, None, img_h, 1)

    act = 'relu'
    input_data = Input(name='input', batch_shape=input_shape, dtype='float32')
    inner = Conv2D(16, (3,3), padding='same',activation=act, kernel_initializer='he_normal',name='conv1')(input_data)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max1')(inner)
    inner = Conv2D(32, (3,3), padding='same',activation=act, kernel_initializer='he_normal',name='conv2')(inner)
    inner = MaxPooling2D(pool_size=(pool_size, pool_size), name='max2')(inner)
    inner = Conv2D(64, (3,3), padding='valid',activation=act, kernel_initializer='he_normal',name='conv3')(inner)
    inner = MaxPooling2D(pool_size=(1, pool_size), name='max3')(inner)
    inner = Dropout(rate=0.5, noise_shape=None, seed=None)(inner)
    inner = Conv2D(128, (3,3), padding='valid',activation=act, kernel_initializer='he_normal',name='conv4')(inner)
    inner = OCRLayers.SqLayer()(inner)
    gru_1 = GRU(rnn_size, return_sequences=True, kernel_initializer='he_normal', name='gru1')(inner)
    gru_1b = GRU(rnn_size, return_sequences=True, go_backwards=True, kernel_initializer='he_normal', name='gru1_b')(inner)
    gru1_merged = add([gru_1, gru_1b])
    gru_2 = GRU(rnn_size, return_sequences=True, kernel_initializer='he_normal', name='gru2')(gru1_merged)
    gru_2b = GRU(rnn_size, return_sequences=True, go_backwards=True, kernel_initializer='he_normal', name='gru2_b')(gru1_merged)
    inner = Dense(len(codec)+1, kernel_initializer='he_normal',  name='dense2')(concatenate([gru_2, gru_2b]))
    y_pred = Activation('softmax', name='softmax')(inner)

    labels = Input(name='labels', shape=[None], dtype='float32')
    input_length = Input(name='input_length', shape=[1], dtype='int64')
    label_length = Input(name='label_length', shape=[1], dtype='int64')

    loss_out = Lambda(ctc_lambda_func, output_shape=(1,), name='ctc')([labels, y_pred, input_length, label_length])
    model = Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_out)
    model.compile(loss={'ctc': lambda y_true, y_pred: y_pred}, optimizer=SGD(lr=0.02, decay=1e-6, momentum=0.9, nesterov=True, clipnorm=5))

    if NacistVahy == True: NactiVahy(model, img_h, codec)
    return (model, input_data, y_pred)
def main(DirData, img_h, codec):
    (model, input_data, y_pred) = DejModel(img_h, codec, NacistVahy=True)
    Model(inputs=input_data, outputs=y_pred).summary()
    if K.learning_phase() == True:
        modelPred = Model(inputs=[input_data], outputs=[y_pred])
        modelPred.compile(loss='binary_crossentropy', optimizer='rmsprop')
        InsGen = Utils.GenClass(Invert=RandomInvert)
        CallFc = Utils.MojeCallback(DirData, codec, modelPred, tf.Session(), NumSteps, DownSample, InsGen)
        model.fit_generator(generator=InsGen.GenNextData(DirData + '/Train', codec, DownSample, Random=True), steps_per_epoch=NumSteps, epochs=NumEpochs, callbacks=[CallFc])
        plot_model(model, to_file='model.png')
        model.save_weights(os.path.basename(sys.argv[0]).split('.')[0] + '.hdf5')
    else:
        model.load_weights(os.path.basename(sys.argv[0]).split('.')[0] + '.hdf5')
        modelPred = Model(inputs=[input_data], outputs=[y_pred])
        modelPred.compile(loss='binary_crossentropy', optimizer='rmsprop')
        Utils.PrintClass.PrintCompare(DirData, os.getcwd(), modelPred, codec, DownSample)
if __name__ == '__main__': main(CestaData, 32, Utils.DejCodec(os.getcwd() + '/codec.txt'))