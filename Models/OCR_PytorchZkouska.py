import torch, sys, os, Utils
sys.path.append('C:/Users/Milan/PycharmProjects/OCR')
sys.path.append('..')
os.chdir(sys.path[0])
from torch.nn import MaxPool2d, LeakyReLU, Conv2d, BatchNorm2d, Dropout2d, Softmax2d
from torch.autograd import Variable
from warpctc_pytorch import CTCLoss
from torch.optim import SGD, Adadelta
from torch.nn.init import normal, xavier_normal
import torch.nn as nn
import numpy as np
from visualize import make_dot

NumEpochs = 60
NumSteps = 300
# CestaData = 'C:/Users/Milan/PycharmProjects/OCR/SynthDataset'
CestaData = '/home/milan/Milan/OCR/SynthDataset'
#CestaData = '/tmp/qqpultar'

# Network parameters
pool_size = 2
RandomInvert = False

def DownSample(x): return (x // (pool_size ** 2) - 4)

class DejModel(nn.Module):
    def __init__(self, codec):
        super(DejModel, self).__init__()
        self.conv1 = Conv2d(3, 32, (3,3), padding=1)
        self.conv2 = Conv2d(32, 64, (3,3), padding=1)
        self.conv3 = Conv2d(64, 128, (3,3), padding=1)
        self.conv4 = Conv2d(128, 256, (3,3))
        self.conv5 = Conv2d(256, len(codec)+1, (2,2))
        self.batch0 = BatchNorm2d(3, eps=1e-05, momentum=0.1, affine=False)
        self.batch1 = BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
        self.batch2 = BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True)
        self.batch3 = BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
        self.batch4 = BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
        self.drop1 = Dropout2d(p=0.5, inplace=False)
        for conv in {self.conv1, self.conv2, self.conv3, self.conv4, self.conv5}:
            xavier_normal(conv.weight.data)
            normal(conv.bias.data)

    def forward(self, x, codec):
        x = self.conv1(x)
        x = self.batch1(x)
        x = MaxPool2d((2, 2), stride=None, padding=0)(x)
        x = LeakyReLU(negative_slope=0.01, inplace=False)(x)
        x = self.conv2(x)
        x = self.batch2(x)
        x = LeakyReLU(negative_slope=0.01, inplace=False)(x)
        x = MaxPool2d((2, 2), stride=None, padding=0)(x)
        x = self.conv3(x)
        x = self.batch3(x)
        x = LeakyReLU(negative_slope=0.01, inplace=False)(x)
        x = MaxPool2d((2, 1), stride=None, padding=0)(x)
        x = self.conv4(x)
        x = self.batch4(x)
        x = LeakyReLU(negative_slope=0.01, inplace=False)(x)
        x = self.conv5(x)
        x = self.drop1(x)
        x = Softmax2d()(x)
        x = x + 1e-08
        x = torch.log(x)
        x = x.squeeze(2)
        return x
def main(DirData, codec):
    model = DejModel(codec)
    model.train(True)
    # optimizer = torch.optim.Adadelta(model.parameters(),lr=1.0, rho=0.9, eps=1e-06, weight_decay=0)
    optimizer = SGD(model.parameters(), lr = 0.001, momentum=0.9, nesterov=True)
    ctc_loss = CTCLoss()
    (inputs, labels) = next(Utils.GenClass(Invert=RandomInvert).GenNextData(DirData + '/Train', codec, DownSample, Random=True))
    make_dot(model(Variable(torch.from_numpy(np.expand_dims(inputs['input'][0], 0)).float()), codec)).render(os.path.basename(sys.argv[0]).split('.')[0])
    gen = Utils.GenClass(Invert=RandomInvert).GenNextData(DirData + '/Train', codec, DownSample, Random=True)
    for epoch in range(NumEpochs):
        for PG in optimizer.param_groups: PG['lr']=PG['lr']*0.9
        print('Epoch {}/{}'.format(epoch, NumEpochs))
        running_loss = 0.0
        for i in range(NumSteps):
            (inputs, labels) = next(gen)
            labels = Variable(torch.from_numpy(inputs['labels'][0]).int() + 1)
            inputs = Variable(torch.from_numpy(np.expand_dims(inputs['input'][0], 0)).float() / 128 - 1)
            optimizer.zero_grad()
            outputs = model(inputs, codec)
            probs_sizes = Variable(torch.IntTensor( [outputs.size()[2]]) )
            label_sizes = Variable(torch.IntTensor( [len(labels)]) )
            loss = ctc_loss(outputs.permute(2,0,1), labels, probs_sizes, label_sizes)
            loss.backward()
            torch.nn.utils.clip_grad_norm(model.parameters(), 5)
            optimizer.step()

            print('\rsample {}/{}, ctc_loss: {}'.format(i,NumSteps,loss.data[0]),end='')
            running_loss += loss.data[0]
        print('\rAvg. loss: %.3f' % (running_loss / NumSteps))
    print('Finished Training')

    # model.save_weights(os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
    # model.load_weights(os.path.basename(sys.argv[0]).split('.')[0]+'.hdf5')
    # Utils.PrintClass.PrintCompare(DirData, os.getcwd(), modelPred, codec, DownSample, Train=False)
if __name__ == '__main__': main(CestaData, Utils.DejCodec(os.getcwd()+'/codec.txt'))