import torch, sys, os
sys.path.append('C:/Users/Milan/PycharmProjects/OCR')
sys.path.append('..')
os.chdir(sys.path[0])
import Utils
from torch.nn import MaxPool2d, LeakyReLU, Conv2d, BatchNorm2d, Dropout2d, Softmax2d, InstanceNorm2d
from torch.autograd import Variable
from warpctc_pytorch import CTCLoss
from torch.optim import SGD, Adadelta, Adagrad
from torch.nn.init import normal, xavier_normal
import torch.nn as nn
import numpy as np
from visualize import make_dot
TrainMode, TestMode = 1, 0

ModelMode = TrainMode
NumEpochs = 60
NumSteps = 900
RandomInvert = False
DatasetName = 'DatasetEnRu1200_400'
if os.getcwd().split('/')[1] == 'home': CestaData = '/home/milan/Milan/OCR_Pytorch/Datasets/'+DatasetName
else: CestaData = '/tmp/qqpultar/'+DatasetName # napr. na Halmos

class DejModel(nn.Module):
    def __init__(self, codec):
        super(DejModel, self).__init__()
        self.conv1 = Conv2d(3, 32, (3,3), padding=1)
        self.conv2 = Conv2d(32, 64, (3,3), padding=1)
        self.conv3 = Conv2d(64, 128, (3,3), padding=1)
        self.conv4 = Conv2d(128, 256, (3,3))
        self.conv5 = Conv2d(256, len(codec)+1, (2,2))
        # self.batch1 = BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
        # self.batch2 = BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True)
        # self.batch3 = BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
        # self.batch4 = BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
        self.batch1 = InstanceNorm2d(32, eps=1e-05, momentum=0.1, affine=True)
        self.batch2 = InstanceNorm2d(64, eps=1e-05, momentum=0.1, affine=True)
        self.batch3 = InstanceNorm2d(128, eps=1e-05, momentum=0.1, affine=True)
        self.batch4 = InstanceNorm2d(256, eps=1e-05, momentum=0.1, affine=True)
        self.drop1 = Dropout2d(p=0.5, inplace=False)
        self.leaky = LeakyReLU(negative_slope=0.01, inplace=False)
        self.max1 = MaxPool2d((2, 2), stride=None, padding=0)
        self.max2 = MaxPool2d((2, 1), stride=None, padding=0)
        for conv in {self.conv1, self.conv2, self.conv3, self.conv4, self.conv5}:
            xavier_normal(conv.weight.data)
            normal(conv.bias.data)
    def forward(self, x, codec):
        try: x = x.cuda(0)
        except: pass
        x = self.conv1(x)
        x = self.batch1(x)
        x = self.leaky(x)
        x = self.max1(x)
        x = self.conv2(x)
        x = self.batch2(x)
        x = self.leaky(x)
        x = self.max1(x)
        x = self.conv3(x)
        x = self.batch3(x)
        x = self.leaky(x)
        x = self.max2(x)
        x = self.conv4(x)
        x = self.batch4(x)
        x = self.leaky(x)
        x = self.drop1(x)
        x = self.conv5(x)
        x = x.squeeze(2)
        x = x.squeeze(0).permute(1,0)
        x = torch.nn.LogSoftmax()(x)
        x = x.permute(1,0).unsqueeze(0)
        return x
def Train(DirData, codec):
    model = DejModel(codec)
    try: model = model.cuda(0)
    except: pass
    # optimizer = torch.optim.Adadelta(model.parameters(),lr=1, rho=0.9, eps=1e-06, weight_decay=0.0)
    # optimizer = torch.optim.Adagrad(model.parameters(), lr=0.01, lr_decay=0, weight_decay=0)
    optimizer = SGD(model.parameters(), lr = 0.001, momentum=0.9, nesterov=True)
    ModelWrap = Utils.ModelClass(model, DirData, codec)
    ModelWrap.TrainModel(RandomInvert, NumEpochs, NumSteps, optimizer, clip_norm=50)
def Test(DirData, codec):
    ModelWrap = Utils.ModelClass(DejModel(codec), DirData, codec)
    ModelWrap.TestModel(torch.load(os.path.basename(sys.argv[0]).split('.')[0]+'.state', map_location=lambda storage, loc: storage))
if __name__ == '__main__':
    if ModelMode == TrainMode: Train(CestaData, Utils.DatasetClass.DejCodec(CestaData+'/codec.txt'))
    elif ModelMode == TestMode: Test(CestaData, Utils.DatasetClass.DejCodec(CestaData+'/codec.txt'))