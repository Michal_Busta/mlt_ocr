import os, glob, sys
sys.path.append('/mnt/textspotter/software/opencv/ReleaseP3/lib/python3')
import numpy as np
from shutil import copyfile
from PIL import Image
import PIL
from scipy.spatial.distance import euclidean
import cv2

class ListOps:
    @staticmethod
    def ChangeFormat(DirName, DirOut, RelTo=None, exclude=['codec.txt','Readme.txt','list.txt']):
        if RelTo==None: RelTo = os.path.dirname(DirOut)
        with open(DirOut,'w') as textF:
            for dir, _, files in os.walk(DirName):
                for file in files:
                    if file.split('.')[1]=='txt' and not file in exclude:
                        # name = file.split('.')[0]
                        relpath = os.path.relpath(dir+'/'+file, RelTo).split('.')[0]+'.png'
                        textF.write(relpath+' '+open(dir+'/'+file,'r').readline())
        print('list saved')
    @staticmethod
    def EraseCommas(DirList): # Do only once !!!
        with open(DirList+'pom','w') as textP:
            for ln in open(DirList,'r').readlines():
                first = ln.split(' ')[0]
                second = ln.strip()[ln.find(' ')+2:-1]+'\n'
                # second = ln.strip()[ln.find(' ')+1:]+'\n'
                textP.write(first+' '+second)
        copyfile(DirList+'pom', DirList)
        os.remove(DirList+'pom')
        print('Done')
    @staticmethod
    def ChangePath(DirList, Old, New): # Not tested yet !!!
        with open(DirList+'pom','w') as textP:
            for ln in open(DirList,'r').readlines():
                first = ln.split(' ')[0]
                second = ln.strip()[ln.find(' ')+1:]+'\n'
                first = first.replace(Old, New)
                textP.write(first+' '+second)
        copyfile(DirList+'pom', DirList)
        os.remove(DirList+'pom')
        print('Done')
        pass
class Dataset:
    @staticmethod
    def PrepareICDAR2013(DirName):
        with open(DirName+'/gt.txt','r') as TextF:
            for ln in TextF.readlines():
                (name, text) = ln.strip().split(' ')
                name = name[:-1].split('.')[0]
                text = text.strip()[1:-1]
                open(DirName+'/'+name+'.txt', 'w').write(text+'\n')
        for file in glob.glob(DirName+'/*.png'):
            img = Image.open(file)
            img = img.resize((int(img.size[0]/(img.size[1]/32)),32),resample=PIL.Image.BILINEAR)
            img.save(file)
    @staticmethod
    def PrepareAmazon(DirName):
        with open(DirName+'/list.txt','r') as TextF:
            for ln in TextF.readlines():
                name = ln.strip().split(',')[0]
                name = name.split('/')[1]
                start = ln.find(' ')+1
                text = ln.strip()[start+1:-1]
                open(DirName+'/'+name+'.txt', 'w').write(text+'\n')
                print('done '+name)
    @staticmethod
    def PrepareChineseDataset(dir):
        NameA, NameB = 'DoneA', 'DoneB'
        os.makedirs(dir+'/../'+NameA, exist_ok=True)
        os.makedirs(dir+'/../'+NameB, exist_ok=True)
        ListA, ListB = list(), list()
        for ind,file in enumerate(sorted(glob.glob(dir+"/*.jpg"))):
            name = os.path.splitext(os.path.basename(file))[0]
            img = Image.open(file)
            for i,ln in enumerate(open(dir+'/'+name+'.txt', 'r', encoding='utf8').readlines()):
                ln = (ln.strip()).split(',')
                text = ln[9][1:-1]
                level = int(ln[8])
                if text=='###' or level == 1: continue
                try:
                    X = [ln[0],ln[2],ln[4],ln[6]]
                    Y = [ln[1],ln[3],ln[5],ln[7]]
                    X = [int(a) for a in X]
                    Y = [int(a) for a in Y]
                    P1 = [X[0],Y[0]]
                    P2 = [X[1],Y[1]]
                    P3 = [X[2],Y[2]]
                    img.crop((min(X),min(Y),max(X),max(Y))).save(dir+'/../'+NameA+'/'+name+'_'+str(i)+'.jpg')
                    pts1 = np.float32([P1, P2, P3])
                    pts2 = np.float32([[0, 0], [euclidean(P1,P2), 0], [euclidean(P1,P2), euclidean(P2,P3)]])
                    affine = cv2.getAffineTransform(pts1, pts2)
                    new = cv2.warpAffine(np.asarray(img), affine, (int(euclidean(P1,P2)), int(euclidean(P2,P3))),borderMode=cv2.BORDER_REPLICATE)
                    Image.fromarray(new).save(dir+'/../'+NameB+'/'+name+'_'+str(i)+'.jpg')
                    ListA.append((os.path.relpath(dir+'/../'+NameA+'/'+name+'_'+str(i)+'.jpg',dir+'/..'), text))
                    ListB.append((os.path.relpath(dir+'/../'+NameB+'/'+name+'_'+str(i)+'.jpg',dir+'/..'), text))
                except:
                    os.remove(dir+'/../'+NameA+'/'+name+'_'+str(i)+'.jpg') if os.path.exists(dir+'/../'+NameA+'/'+name+'_'+str(i)+'.jpg') else None
                    os.remove(dir+'/../'+NameB+'/'+name+'_'+str(i)+'.jpg') if os.path.exists(dir+'/../'+NameB+'/'+name+'_'+str(i)+'.jpg') else None
            if (ind+1)%100==0: print('Pic '+str(ind+1))
        with open(dir+'/../ListA.txt','w') as TextF:
            for sample in ListA: TextF.write(sample[0]+' '+sample[1]+'\n')
        with open(dir+'/../ListB.txt','w') as TextF:
            for sample in ListB: TextF.write(sample[0]+' '+sample[1]+'\n')
        print('Done')
    # def prepareData(): # vzato z MLZkouska.py
    #     cwd = os.getcwd() + '\\book'
    #     Rpom = Rkam = os.getcwd() + '\\Dataset\\'
    #     os.makedirs(Rpom + 'Train\\', exist_ok=True)
    #     os.makedirs(Rpom + 'Test\\', exist_ok=True)
    #     #print(len(*[glob.glob(cwd + "\\book" + "\\*")]))
    #     pom = 1
    #     for root, dirs, files in os.walk(cwd):
    #         for file in files:
    #             if file.endswith('.txt'):
    #                 if pom <= 300:
    #                     Rpom = Rkam + 'Train\\'
    #                 else:
    #                    Rpom = Rkam + 'Test\\'
    #                 print(os.path.join(root, file))
    #                 copyfile(os.path.join(root, file), Rpom + str(pom) + '.txt')
    #                 copyfile(os.path.join(root, os.path.splitext(os.path.splitext(file)[0])[0] + '.bin.png'), Rpom + str(pom) + '.bin.png')
    #                 pom+=1
    # def VytvorDataset(dirname, TrainSize):
    #     cesta = os.path.dirname(dirname)+'\Train'
    #     os.makedirs(cesta, exist_ok=True)
    #     index = 1
    #     for root, dirs, files in os.walk(dirname):
    #         if index > TrainSize:
    #             cesta = os.path.dirname(dirname)+'\Test'
    #             os.makedirs(cesta, exist_ok=True)
    #         for file in files:
    #             if file.endswith('.png'):
    #                 name = os.path.splitext(os.path.splitext(os.path.basename(file))[0])[0]
    #                 copyfile(root+'/'+file, cesta+'/'+str(index)+'.png')
    #                 copyfile(root+'/'+name+'.gt.txt', cesta+'/'+str(index)+'.txt')
    #                 index += 1
    #     return
if __name__ == '__main__':
    # PrepareICDAR2013('/home/milan/Milan/OCR_Pytorch/icdar2013-recognition')
    # PrepareAmazon('/home/milan/Milan/OCR_Pytorch/Datasets/Amazon')
    # Dataset.ChangeFormat('/home/milan/Milan/OCR_Pytorch/Datasets/Dataset_EnRu_1700_600/Train',
    #                      DirOut='/home/milan/Milan/OCR_Pytorch/Datasets/Dataset_EnRu_1700_600/ListTrain.txt')
    # Dataset.EraseCommas('/home/milan/Milan/OCR_Pytorch/Datasets/Amazon/ListTrain.txt')
    # Dataset.PrepareChineseDataset('/home/milan/Milan/OCR_Pytorch/Datasets/CinstinaData')
    Dataset.PrepareChineseDataset('/mnt/datagrid/personal/qqpultar/OCR/Datasets/ChineseDS_Train')
    pass