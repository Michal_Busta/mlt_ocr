import PIL
from PIL import ImageFont, Image, ImageDraw
import random, glob, os
from random import randint
import unicodedata
from shutil import move

class GetOutOfLoop(Exception): pass
class SynthCreateClass():
    def __init__(self, DirFonts, DirBacks, DirText, NameOut, Words=(1,6), MaxWords=10000, MaxPages=3):
        self.DirFonts = DirFonts
        self.DirBacks = DirBacks
        self.NameOut = NameOut
        self.DirText = DirText
        self.Words = Words
        self.MaxWords = MaxWords
        self.MaxPages = MaxPages
        self.slova = list()
        try:
            for (ind,file) in enumerate(sorted(glob.glob(self.DirText+'/*'))):
                if ind == MaxPages: raise GetOutOfLoop
                with open(file,'r',encoding='utf8') as textIn:
                    for ln in textIn.readlines():
                        ln = ln.strip()
                        ln = unicodedata.normalize('NFKD', ln) # odstranime accents
                        ln = u"".join([c for c in ln if not unicodedata.combining(c)])
                        if len(ln) > 0:
                            self.slova = self.slova + ln.split(' ')
                            if len(self.slova)>=self.MaxWords: raise GetOutOfLoop
                    print('Read text '+str(ind+1)+', words: '+str(len(self.slova)))
        except GetOutOfLoop: pass
        print('Text ready, words: '+str(len(self.slova)))
    def __call__(self, NumSamples=-1, StartIndex=1, RandPad=(5,20), RandShift=(-2,2)):
        FontsList = sorted(glob.glob(self.DirFonts+'/*.ttf'))
        BacksList = sorted(glob.glob(self.DirBacks+'/*.jpg'))
        os.makedirs(os.getcwd()+'/'+self.NameOut, exist_ok=True)
        samples = list()
        for (i,text) in enumerate(self.GenText(NumSamples=NumSamples)):
            PadLeft, PadRight = randint(RandPad[0],RandPad[1]), randint(RandPad[0],RandPad[1])
            font = ImageFont.truetype(random.choice(FontsList), 32)
            with Image.open(random.choice(BacksList)).resize((font.getsize(text)[0]+PadLeft+PadRight,font.getsize(text)[1]), resample=PIL.Image.NEAREST) as img:
                draw = ImageDraw.Draw(img)
                draw.text((PadLeft, randint(RandShift[0],RandShift[1])),text,(0,0,0),font=font)
                img = img.resize((img.size[0],32), resample=PIL.Image.NEAREST)
                img.save(os.getcwd()+'/'+self.NameOut+'/'+str(i+StartIndex)+'.png','PNG')
                # with open(os.getcwd()+'/'+self.NameOut+'/'+str(i+StartIndex)+'.txt','w',encoding='utf8') as textF: textF.write(text+'\n'+str(font.getname()))
                samples.append((os.path.relpath(os.getcwd()+'/'+self.NameOut+'/'+str(i+StartIndex)+'.png', self.NameOut+'/../'), text))
            if (i+1) % 100 == 0: print('Pic '+str(i+1))
        with open(self.NameOut+'/../'+'list.txt','w') as TextF:
            for sm in samples: TextF.write(sm[0]+' '+sm[1]+'\n')
        print('DONE')
    def GenText(self, NumSamples=-1):
        i = 0
        while NumSamples==-1 or i < NumSamples:
            Zind = randint(0,len(self.slova)-1)
            Kind = min(len(self.slova)-1, Zind+randint(self.Words[0],self.Words[1]))
            yield ' '.join(self.slova[Zind:Kind])
            i+=1
    @staticmethod # WARNING: it may overwrite some of your files if abs(change) is too small
    def PosunCislovani(DirName, change):
        SFiles = list()
        for file in sorted(glob.glob(DirName)): SFiles.append(file)
        for file in SFiles:
            name = int(os.path.basename(file).split('.')[0])
            dir = os.path.dirname(file)
            ext = os.path.basename(file).split('.')[1]
            move(dir+'/'+str(name)+'.'+ext,dir+'/'+str(name+change)+'.'+ext)
if __name__ == '__main__':
    syn = SynthCreateClass(os.getcwd()+'/Fonts/GoogleFonts',os.getcwd()+'/Backgrounds/Bright',os.getcwd()+'/Texts/Ru','SynthRu6000', Words=(1,7), MaxWords=30000, MaxPages=5)
    syn(6000,StartIndex=1, RandPad=(1,10), RandShift=(-2,2))
    # SynthCreateClass.PosunCislovani(os.getcwd()+'/NewSynths/*', -300)