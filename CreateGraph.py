import numpy as np
import matplotlib.pyplot as plt

# TrainSenAcc, TestSenAcc, TrainWordAcc, TestWordAcc, TrainLevAcc, TestLevAcc
DataUW = [#('E1',[0.89666666, 0.47236180, 0, 0, 0, 0]),
        #('E6(Bnorm)',[0.9933333, 0.37185929, 0, 0, 0, 0]),
        ('E7(GRU)',[0.86666666, 0.4333333, 0.98765432, 0.882882882882, 0, 0]),
        # ('E8(dropout)',[0.9, 0.633333333, 0.990950226, 0.93421052631, 0, 0]),
        ('E10(paper)',[0.77, 0.49748743718, 0.9521410579, 0.88654891, 0, 0]),
        ('E14(PReLU)',[0.8533333, 0.5527638190, 0.96725440, 0.9001358, 0, 0]),
        ('E16(LReLU)',[0.9266666666666666,0.6331658291457286,0.9878253568429891,0.9252717391304348, 0, 0]),
        ('E17(SELU)',[0.14667,0.09548,0.69941,0.6841,0.88293,0.88004]),
        ('E19(SepCnv)',[0.01, 0.0, 0.23174, 0.2337, 0.67397, 0.69352]),
        ('E20(DpOut)',[0.80333, 0.58291, 0.97355, 0.90965, 0.98946, 0.9766]),
        ('E21(E20+Inv)',[0.55333, 0.36683, 0.91226, 0.85054, 0.96664, 0.95508]),
        ('E22(Paper)',[0.92,0.61809,0.9916,0.92595,0.9973,0.98135]),
        ('E23(E22+inv)',[0.23,0.11558,0.77246,0.71196,0.93925,0.92043])]
DataRU = [('E24(Basic)',[0.99667,0.775,0.99853,0.94124,0.9998,0.98394]),
        ('E25(paper)',[1.0,0.705,1.0,0.92542,1.0,0.98124])]
DataEnRu = [('E29(BN)600/400', [0.65167,0.38847,0.91822,0.82942,0.98178,0.95901]),
            ('E30(BN)1200/400', [0.41833,0.21303,0.72489,0.68357,0.90164,0.90312]),
            ('E33(IN)En300/200',[1.0,0.55779,1.0,0.90421,1.0,0.97903]),
            ('E32(IN)600/400',[1.0,0.62406,1.0,0.90277,1.0,0.97668]),
            ('E31(IN)1200/400',[0.99667,0.63659,0.99858,0.89941,0.99978,0.97693]),
            ('E34(INDP)1200/400',[0.97667,0.6792,0.99035,0.91115,0.99708,0.97995])]

def autolabel(ax, rects):
    for rect in rects:
        height = rect.get_height()
        if height != 0: ax.text(rect.get_x() + rect.get_width()/2., 1.02*height,'%0.2f' % height,ha='center', va='bottom')
def PlotData(Data, Title, OutName=None):
    Exps = [a[0] for a in Data]
    Data = [a[1] for a in Data]
    Data = np.swapaxes(np.asarray(Data), 0,1)
    groups = len(Data[0])

    fig = plt.figure(figsize=(14, 6.5))
    fig.suptitle(Title, fontsize=20)
    index = np.arange(groups)
    BWidth = 1/(groups*3+1)
    index = np.asarray([BWidth + i*3*BWidth for i in range(groups)])

    ax = plt.subplot(311)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,box.width * 0.9, box.height * 0.8])
    rects1 = plt.bar(index, Data[0], BWidth,color='darkblue',label='Train sen. acc.')
    rects2 = plt.bar(index + BWidth, Data[1], BWidth,color='dodgerblue',label='Test sen. acc.')
    autolabel(ax, rects1)
    autolabel(ax, rects2)
    plt.xticks(index + BWidth*(1/2), Exps)
    # plt.legend(loc='upper center', fancybox=True, shadow=True,ncol=1,bbox_to_anchor=(1.1, 0.5))
    plt.legend(loc='upper center', ncol=1,bbox_to_anchor=(1.1, 0.5))
    plt.ylabel('accuracy')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax = plt.subplot(312)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,box.width * 0.9, box.height * 0.8])
    rects3 = plt.bar(index, Data[2], BWidth,color='darkred',label='Train word acc.')
    rects4 = plt.bar(index + BWidth, Data[3], BWidth,color='r',label='Test word acc.')
    autolabel(ax, rects3)
    autolabel(ax, rects4)
    plt.xticks(index + BWidth*(1/2), Exps)
    # plt.legend(loc='upper center', fancybox=True, shadow=True,ncol=2,bbox_to_anchor=(0.5, -0.09))
    plt.legend(loc='upper center', ncol=1,bbox_to_anchor=(1.1, 0.5))
    plt.ylabel('accuracy')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax = plt.subplot(313)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,box.width * 0.9, box.height * 0.8])
    rects3 = plt.bar(index, Data[4], BWidth,color='darkgreen',label='Train Lev. acc.')
    rects4 = plt.bar(index + BWidth, Data[5], BWidth,color='limegreen',label='Test Lev. acc.')
    autolabel(ax, rects3)
    autolabel(ax, rects4)
    plt.xticks(index + BWidth*(1/2), Exps)
    plt.legend(loc='upper center', ncol=1,bbox_to_anchor=(1.1, 0.5))
    plt.ylabel('accuracy')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    if OutName!=None: fig.savefig(OutName)
    plt.show()

if __name__ == '__main__':
    # PlotData(DataUW, "UW500", OutName='SumUW')
    # PlotData(DataRU, "Ru_300/200 Synth Dataset", OutName='SumRU')
    PlotData(DataEnRu, "EnRu Synth Dataset", OutName='SumEnRu')