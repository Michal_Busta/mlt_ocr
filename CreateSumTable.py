import diff_match_patch as DMP
from prettytable import PrettyTable
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import table
import subprocess
import imgkit

name = 'SumTable'
# Data.append(['','','','','','',''])
Data = list()
Data.append(['E20','En','-','300','200','0.8','0.58','0.97','0.91','0.99','0.98','0.5','Batch_norm','TFlow','SimpleNN'])
Data.append(['E22','En','-','300','200','0.92','0.62','0.99','0.93','1.0','0.98','0.5','Batch_norm','TFlow','PaperNN'])

Data.append(['E24','Ru','-','300','200','1.0','0.78','1.0','0.94','1.0','0.98','0.5','Batch_norm','TFlow','SimpleNN'])
Data.append(['E25','Ru','-','300','200','1.0','0.7','1.0','0.93','1.0','0.98','0.5','Batch_norm','TFlow','PaperNN'])

Data.append(['E29','En+Ru','-','600','400','0.65','0.39','0.92','0.83','0.98','0.96','0.2','Batch_norm','Pytorch','PaperNN'])
Data.append(['E30','En+Ru','-','1200','400','0.42','0.21','0.72','0.68','0.90','0.90','0.2','Batch_norm','Pytorch','PaperNN'])
Data.append(['E33','En','-','300','200','1.0','0.56','1.0','0.9','1.0','0.98','0.2','InstanceNorm','Pytorch','PaperNN'])
Data.append(['E32','En+Ru','-','600','400','1.0','0.62','1.0','0.9','1.0','0.98','0.2','InstanceNorm','Pytorch','PaperNN'])
Data.append(['E31','En+Ru','-','1200','400','1.0','0.64','1.0','0.9','1.0','0.98','0.2','InstanceNorm','Pytorch','PaperNN'])
Data.append(['E34','En+Ru','-','1200','400','0.98','0.68','0.99','0.91','1.0','0.98','0.5','InstanceNorm','Pytorch','PaperNN'])

Data.append(['E35','En+Ru','-','1200','400','0.27','0.15','0.64' ,'0.62' ,'0.90','0.90','0.1','BatchNorm','Pytorch','SimpleNN'])
Data.append(['E36','En+Ru','-','1200','400','0.99','0.59','1.0','0.89','1.0','0.98','0.2','InstanceNorm','Pytorch','SimpleNN'])

Data.append(['E37','En+Ru','DatasetEnRu1200_400New','1200','400','0.96414','0.605','0.98797','0.87795','0.9957','0.96965','0.5','InstanceNorm','Pytorch','PaperNN'])

df = pd.DataFrame(columns=['Name','Script','Dataset','size<br>Train','size<br>Test','SenAcc<br>Train','SenAcc<br>Test',
                           'WordAcc<br>Train','WordAcc<br>Test','LevAcc<br>Train','LevAcc<br>Test','Drop-<br>out','Norm-layers','Package','Comment'])
for (i,nxt) in enumerate(Data):
    df.loc[i] = nxt
with open('SumTable.html', 'w') as textF: textF.write(df.to_html(escape=False))
imgkit.from_file(name+'.html', name+'.jpg')

# subprocess.call('wkhtmltoimage -f png --width 0 SumTable.html SumTable.png', shell=True)

# fig, ax = plt.subplots(1, 1)
# ax.get_xaxis().set_visible(False)   # Hide Ticks
# df.plot(table=True, ax=ax)


# ax = plt.subplot(111, frame_on=False) # no visible frame
# ax.xaxis.set_visible(False)  # hide the x axis
# ax.yaxis.set_visible(False)  # hide the y axis
# table(ax, df, rowLabels=['']*df.shape[0], loc='center')  # where df is your data frame
# plt.savefig('mytable.png')

# fig, ax = plt.subplots(figsize=(12, 2)) # set size frame
# ax.xaxis.set_visible(False)  # hide the x axis
# ax.yaxis.set_visible(False)  # hide the y axis
# ax.set_frame_on(False)  # no visible frame, uncomment if size is ok
# tabla = table(ax, df, loc='upper right', colWidths=[0.17]*len(df.columns))  # where df is your data frame
# tabla.auto_set_font_size(False) # Activate set fontsize manually
# tabla.set_fontsize(12) # if ++fontsize is necessary ++colWidths
# tabla.scale(1.2, 1.2) # change size table
# plt.savefig('table.png', transparent=True)